import React from 'react'
import { Text, View } from 'react-native'
import Header from '../Components/Header'
import PlayList from '../Components/PlayList'
import Slider from '../Components/Slider'
import VideoGridList from '../Components/VideoGridList'
import Color from '../Shared/Color'

function HomeScreen() {
  return (
   <View style={{ backgroundColor:Color.black}}>
        <Header/>
        <Slider/>
        <PlayList/>
        <VideoGridList/>
   </View>
  )
}

export default HomeScreen