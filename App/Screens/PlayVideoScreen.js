import { useRoute } from '@react-navigation/native'
import React, { useEffect } from 'react'
import { View } from 'react-native'
import PlayList from '../Components/PlayList'
import SubscribeSection from '../Components/SubscribeSection'
import VideoGridList from '../Components/VideoGridList'
import YTPlayer from '../Components/YTPlayer'
import Color from '../Shared/Color'

function PlayVideoScreen() {
    const param=useRoute().params;
    useEffect(()=>{
        
    },[])
  return (
    <View style={{ backgroundColor:Color.black,flex:1}}>
        <YTPlayer param={param}/>
        <SubscribeSection/>
        <PlayList/>
        <VideoGridList/>
    </View>
  )
}

export default PlayVideoScreen