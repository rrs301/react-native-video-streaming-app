import { createNativeStackNavigator } from '@react-navigation/native-stack'
import React from 'react'
import { StyleSheet } from 'react-native';
import HomeScreen from '../Screens/HomeScreen';
import PlayVideoScreen from '../Screens/PlayVideoScreen';

const Stack=createNativeStackNavigator();
function HomeScreenNavigation() {
  return (
        <Stack.Navigator screenOptions={{headerShown:false}}
        >
            <Stack.Screen component={HomeScreen}
            name='home'></Stack.Screen>
            <Stack.Screen component={PlayVideoScreen} name='playVideo'></Stack.Screen>
       
        </Stack.Navigator>
  )
}



export default HomeScreenNavigation