import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import Color from '../Shared/Color'

function Header() {
    const userImage="https://w7.pngwing.com/pngs/340/946/png-transparent-avatar-user-computer-icons-software-developer-avatar-child-face-heroes.png"
  return (
    <View style={styles.container}>
        <View>
            <Text style={styles.welcomeText}>Welcome,</Text>
            <Text style={styles.userNameText}>Rahul Sanap</Text> 
        </View>
        <Image source={{uri:userImage}} style={styles.userImage} />
    </View>
  )
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    userImage:{
        width:45,
        height:45,
        borderRadius:50
    },
    welcomeText:{
        fontSize:20,
        fontWeight:'bold',
        color:Color.white
    },
    userNameText:{
        color:Color.white
    }
})

export default Header