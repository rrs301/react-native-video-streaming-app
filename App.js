import { DefaultTheme, NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import HomeScreenNavigation from './App/Navigation/HomeScreenNavigation';
import HomeScreen from './App/Screens/HomeScreen';
import PlayVideoScreen from './App/Screens/PlayVideoScreen';
import Color from './App/Shared/Color';

const navTheme={
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    flex: 1,
    padding:20,
    marginTop:20,
    backgroundColor:Color.black
  },
}
export default function App() {
  return (
    <NavigationContainer theme={navTheme}>
      
      <View style={styles.container}>
      <HomeScreenNavigation/>
      </View>
      {/* <PlayVideoScreen/> */} 
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
     padding:20,
    marginTop:20,
    backgroundColor:Color.black
  
  },
});
